package com.idigital.epam.energy.payload;


import lombok.*;

import javax.persistence.Entity;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class EnergyMeterRequest {

    private Long homeId;
    private String residential;
    private String institutional;
    private Integer energyConsumption;


}
