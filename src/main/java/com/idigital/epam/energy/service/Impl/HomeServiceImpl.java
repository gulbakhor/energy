package com.idigital.epam.energy.service.Impl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.idigital.epam.energy.entity.Address;
import com.idigital.epam.energy.entity.Home;
import com.idigital.epam.energy.entity.User;
import com.idigital.epam.energy.enums.BuildingType;
import com.idigital.epam.energy.hmac.HMACUtilsService;
import com.idigital.epam.energy.repository.HomeRepository;
import com.idigital.epam.energy.repository.UserRepository;
import com.idigital.epam.energy.service.*;
import com.idigital.epam.energy.service.DTO.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service

public class HomeServiceImpl implements HomeService, CommonService<Home> {

    Logger log = LoggerFactory.getLogger("HomeServiceImpl");
    @Autowired
    HomeRepository homeRepository;

    @Autowired
    HMACUtilsService hmACUtils;

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AddressService addressService;

    @Autowired
    ResidentService residentService;

    public static final String KEY_ID = "ENERGY";
    public static final String SECRET_KEY = "energyKey";
    public static final String REQUEST_PATH = "http://citymanagementbackend-env-1.eba-3swwhqnr.us-east-2.elasticbeanstalk.com/api/v1/request/homesWithOwner";

    @Override
    public List<Home> getAll() {
        return homeRepository.findAll();
    }



    @Override
    public List<HomeResponse> getHomesList(){
        return homeRepository.getAllHoMes();
    }

    @Override
    public List<HomeResponse> getAllHomes(BuildingType buildingType) {
        List<Home> homes = homeRepository.findAllByBuildingType(buildingType);
        List<HomeResponse> homeResponses = new ArrayList<>();
        for (Home home: homes) {
            HomeResponse resp = HomeResponse
                    .builder()
                    .buildingType(home.getBuildingType())
                    .cardNumber(home.getUser() != null ? home.getUser().getCardNumber():"Not Registered")
                    .homeCode(home.homeCode)
                    .homeNumber(home.getAddress().getHomeNumber())
                    .district(home.getAddress().getDistrict())
                    .street(home.getAddress().getStreet())
                    .id(home.id)
                    .build();
            homeResponses.add(resp);
        }
        return homeResponses;
    }

    @Override
    public Home changeHomeOwner(long userId, long homeId) {
        Optional<Home> home = homeRepository.findById(homeId);
        Optional<User> us = userRepository.findById(userId);
        if(home.isPresent() && us.isPresent()){
            home.get().setUser(us.get());
            return homeRepository.save(home.get());
        }
        return null;
    }

    @Override
    public Home getById(Long id) {
        Optional<Home> home = homeRepository.findById(id);
        return home.orElse(null);
    }

    @Override
    public Home create() throws Exception{
        String aas = residentService.getHmacRequest(KEY_ID, "get_homes", REQUEST_PATH, SECRET_KEY);
        ObjectMapper mapper = new ObjectMapper();
        Response json = mapper.readValue(aas, Response.class);
        Home home;
        Address address;
        List<Result> homes = json.getResult();
        for (Result result : homes) {
            for (HomeDto homeDto : result.getHomes()) {
                home = new Home();
                Optional<User> us = userRepository.findUserByCardNumber(result.getCardNumber().toString());
                if (us.isPresent()) {
                    home.setUser(us.get());
                }
                Optional<Home> home1 = homeRepository.findByHomeCodeAndBuildingType(homeDto.getHomeCode(),BuildingType.RESIDENTIAL);
                if(!home1.isPresent()){
                    home.setHomeCode(homeDto.getHomeCode());
                    home.setBuildingType(BuildingType.RESIDENTIAL);
                    address = new Address();
                    address.setHomeNumber(homeDto.getAddress().getHomeNumber());
                    address.setDistrict(homeDto.getAddress().getDistrict());
                    address.setStreet(homeDto.getAddress().getStreet());
                    address = addressService.createAddress(address);
                    home.setAddress(address);
                }else{
                    home.setId(home1.get().getId());
                    home.setHomeCode(home1.get().getHomeCode());
                    home.setBuildingType(home1.get().getBuildingType());
                    home.setAddress(home1.get().getAddress());
                }
                homeRepository.save(home);
            }
        }
        return null;
    }

    @Override
    public Home createInstitutional() throws Exception {
        String institution =  residentService.getHmacRequest(KEY_ID,"get_institutional",REQUEST_PATH, SECRET_KEY);
        ObjectMapper mapper = new ObjectMapper();
        Response json = mapper.readValue(institution, Response.class);
        Home home;
        Address address;
        List<Result> homes = json.getResult();
        for (Result result : homes) {
            for (HomeDto homeDto : result.getHomes()) {
                home = new Home();
                Optional<User> us = userRepository.findUserByCardNumber(result.getCardNumber().toString());
                if (us.isPresent()) {
                    home.setUser(us.get());
                }
                Optional<Home> home1 = homeRepository.findByHomeCodeAndBuildingType(homeDto.getHomeCode(),BuildingType.INSTITUTIONAL);
                if(!home1.isPresent()){
                    home.setHomeCode(homeDto.getHomeCode());
                    home.setBuildingType(BuildingType.INSTITUTIONAL);
                    address = new Address();
                    address.setHomeNumber(homeDto.getAddress().getHomeNumber());
                    address.setDistrict(homeDto.getAddress().getDistrict());
                    address.setStreet(homeDto.getAddress().getStreet());
                    address = addressService.createAddress(address);
                    home.setAddress(address);
                }else{
                    home.setId(home1.get().getId());
                    home.setHomeCode(home1.get().getHomeCode());
                    home.setBuildingType(home1.get().getBuildingType());
                    home.setAddress(home1.get().getAddress());
                }
                homeRepository.save(home);
            }
        }
        return null;
    }


    @Override
    public Home update(Home data) throws Exception{
        if(data.getId() != null){
            Home newHome = homeRepository.findById(data.getId()).get();
            newHome.setHomeCode(data.homeCode);
            newHome.setBuildingType(data.getBuildingType());
            newHome.setAddress(data.getAddress());
            newHome.setUser(data.getUser());
            return homeRepository.save(data);
        }
        throw new Exception("Id shouldn't be null");
    }
    @Override
    public void delete(Home data) {
        // TODO : homeRepository.deleteById(data.getId());
    }

    public Result getHomesByCardNumber(Long cardNumber){
        return new Result(); // TODO : home by card number
    }

    public List<HomeDto> parser(List<Home> homes){
        List<HomeDto> homeDtos = new LinkedList<>();
        HomeDto h;
        for (Home home : homes) {
            h = new HomeDto();
            h.setHomeCode(home.getHomeCode());
            h.setAddress(home.getAddress());
            homeDtos.add(h);
        }
        return homeDtos;
    }
}







