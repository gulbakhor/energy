package com.idigital.epam.energy.service.Impl;

import com.google.gson.Gson;
import com.idigital.epam.energy.entity.Role;
import com.idigital.epam.energy.entity.User;
import com.idigital.epam.energy.hmac.HMACUtilsService;
import com.idigital.epam.energy.repository.RoleRepository;
import com.idigital.epam.energy.repository.UserRepository;
import com.idigital.epam.energy.security.JwtUtil;
import com.idigital.epam.energy.security.UserMaxsus;
import com.idigital.epam.energy.security.UserProvider;
import com.idigital.epam.energy.service.DTO.ResponseResident;
import com.idigital.epam.energy.service.ResidentService;
import com.idigital.epam.energy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    HMACUtilsService hmacUtils;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserProvider userProvider;

    @Autowired
    private JwtUtil jwtTokenUtil;

    @Autowired
    private ResidentService residentService;

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }



    @Override
    public User getById(Long id) {
        return Optional.of(userRepository.findById(id)).filter(Optional::isPresent).get().get();
    }


    @Override
    public String authentication(UserMaxsus userMaxsus) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userMaxsus.getUsername(), userMaxsus.getPassword()));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        } catch (Exception ex){
            ex.printStackTrace();
        }
        UserMaxsus userDetails = userProvider.loadUserByUsername(userMaxsus.getUsername());
        String token  = jwtTokenUtil.generateToken(userDetails, userMaxsus.isRememberMe());
        return token;
    }

    @Override
    public User create(UserMaxsus userMaxsus) throws Exception {

        try {
            String keyId = "ENERGY";
            String action = "get_resident";
            String path = "http://citymanagementbackend-env-1.eba-3swwhqnr.us-east-2.elasticbeanstalk.com/api/v1/resident/card/" + userMaxsus.getUsername();
            String secretKey = "energyKey";
            String resident = residentService.getHmacRequest(keyId,action,path,secretKey);
            ResponseResident user = new Gson().fromJson(resident, ResponseResident.class);
            Optional<User> userOptional = userRepository.findUserByCardNumber(userMaxsus.getUsername());
            if (!userOptional.isPresent()){
                User u = new User();
                u.setActive(user.getResult().getActive());
                u.setFirstName(user.getResult().getFirstName());
                Set<Role> roles = new HashSet<>(userMaxsus.getLavozimlar());
                u.setRole(roles);
                u.setLastName(user.getResult().getLastName());
                u.setPassword(passwordEncoder.encode(userMaxsus.getPassword()));
                u.setCardNumber(String.valueOf(user.getResult().getCardNumber()));
                u = userRepository.save(u);
                u.setPassword(null);
                return u;
            }else{
                throw new Exception("User already exist");
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User getCurrentUser() {
        String username = getPrincipal();
        if (username != null){
            return Optional.of(userRepository.findUserByCardNumber(username)).filter(Optional::isPresent).get().get();
        }
        return null;
    }
    private String getPrincipal() {
        String userName = null;
        Authentication authentication =SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }

    @Override
    public User update(User data) throws Exception {
        return null;
    }

    @Override
    public void delete(User data) {

    }
}
