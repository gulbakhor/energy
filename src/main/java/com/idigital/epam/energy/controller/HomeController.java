package com.idigital.epam.energy.controller;

import com.idigital.epam.energy.entity.Home;
import com.idigital.epam.energy.entity.User;
import com.idigital.epam.energy.enums.BuildingType;
import com.idigital.epam.energy.repository.HomeRepository;
import com.idigital.epam.energy.service.DTO.HomeResponse;
import com.idigital.epam.energy.service.HomeService;
import com.idigital.epam.energy.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/home")
@CrossOrigin(origins = "*", maxAge = 3600)
public class HomeController {

    Logger log = LoggerFactory.getLogger("HomeController");
    @Autowired
    HomeService homeService;
    @Autowired
    HomeRepository homeRepository;

    @Autowired
    UserService userService;


    @GetMapping("/getAllUsersWithHomes")
    public ResponseEntity<List<Home>> getAll() throws Exception {
        homeService.create();
        homeService.createInstitutional();
        List<Home> homes = homeService.getAll();
        return new ResponseEntity(homes, HttpStatus.OK);
    }


    @GetMapping("/getHomesWithCurrentUser")
    public ResponseEntity getResultByCardNumber(){
        User user = userService.getCurrentUser();
        log.warn(" Current User ===> {}", user);
        String card = user.getCardNumber();
        log.warn(" Current User Card number ===> {}", card);
        return ResponseEntity.ok(homeRepository.findHomeListByUserId(card));
    }

    @GetMapping("/getAllRegisteredHomesWithUser")
    public ResponseEntity<List<HomeResponse>> getHomesList() {
        return ResponseEntity.ok(homeService.getHomesList());
    }

    @GetMapping("/getAllHomes")
    public ResponseEntity<List<HomeResponse>> getAllHomes(@RequestParam BuildingType buildingType) {
        log.warn(buildingType.name());
        return ResponseEntity.ok(homeService.getAllHomes(buildingType));
    }

    @PostMapping("/changeHomeOwner")
    public ResponseEntity changeHomeOwner(@RequestParam long userId, @RequestParam long homeId){
        Home home = homeService.changeHomeOwner(userId,homeId);
        return ResponseEntity.ok(home);
    }



//    @GetMapping("/getInstitutional")
//    public ResponseEntity<List<Home>> getInstitutional() throws Exception {
//        homeService.createInstitutional();
//        List<Home> homes = homeService.getAll();
//        return new ResponseEntity(homes, HttpStatus.OK);
//    }

}






//
//    @GetMapping("/{id}")
//    public  ResponseEntity<Home>getHomeById(@PathVariable Long id){
//        Home home = homeService.getById(id);
//        if(home !=null){
//            return new ResponseEntity<>(home,HttpStatus.OK);
//        }else {
//            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST);
//        }
//    }
//
//    @PutMapping("/")
//    public ResponseEntity<Home> updateHome(@RequestBody Home home){
//        try {
//            Home updatedHome=homeService.update(home);
//            return new ResponseEntity<>(updatedHome,HttpStatus.OK);
//        }catch(Exception homeException)
//        {
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,homeException.getMessage());
//        }
//    }
//
//    @DeleteMapping("/{id}")
//    public void delete(@PathVariable Long id) {
//        homeRepository.deleteById(id);
//    }
//
//
//
//
//
//
//    }






