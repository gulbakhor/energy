package com.idigital.epam.energy.service;


import com.google.gson.Gson;
import com.idigital.epam.energy.common.CommonTestObjects;
import com.idigital.epam.energy.entity.Address;
import com.idigital.epam.energy.entity.Home;
import com.idigital.epam.energy.entity.User;
import com.idigital.epam.energy.repository.HomeRepository;
import com.idigital.epam.energy.repository.UserRepository;
import com.idigital.epam.energy.service.DTO.HomeResponse;
import com.idigital.epam.energy.service.Impl.HomeServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

public class HomeServiceImplTest {
    @InjectMocks
    HomeServiceImpl homeService;

    @Mock
    HomeRepository homeRepository;

    @Mock
    UserService userService;

    @Mock
    AddressService addressService;

    @Mock
    ResidentService residentService;

    @Mock
    UserRepository userRepository;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.openMocks(this);
        when(userRepository.findUserByCardNumber(any())).thenReturn(Optional.of(CommonTestObjects.getUserObject()));
    }

    @Test
    @DisplayName("test get all homes")
    public void getAll() {
        List<Home> homes = new ArrayList<>(Collections.singletonList(CommonTestObjects.getHomeTestObject()));
        when(homeRepository.findAll()).thenReturn(homes);
        List<Home> res = homeService.getAll();
        assertNotNull(homes);
        assertEquals(res, homes);
    }

    @Test
    @DisplayName("test get all homes listing")
    public void getHomesList() {
        List<HomeResponse> homeResponseList = new ArrayList<>(Collections.singletonList(CommonTestObjects.getHomeResponseTestObject()));
        when(homeRepository.getAllHoMes()).thenReturn(homeResponseList);
        List<HomeResponse> res = homeService.getHomesList();
        assertNotNull(homeResponseList);
        assertEquals(res, homeResponseList);
    }

    @Test
    public void getById() {
        when(homeRepository.findById(anyLong())).thenReturn(Optional.ofNullable(CommonTestObjects.getHomeTestObject()));
        Home resp = homeService.getById(1L);
        assertNotNull(resp);
    }

    @Test
    public void getByIdWhereHomeIsNull() {
        when(homeRepository.findById(anyLong())).thenReturn(Optional.empty());
        Home resp = homeService.getById(1L);
        assertNull(resp);
    }

    @Test
    public void create() throws Exception {
        Address address = CommonTestObjects.getAddressObject();
        User user = new User();
        user.setActive(true);
        user.setId(1L);
        Home home = CommonTestObjects.getHomeTestObject();
        home.setUser(user);
        when(residentService.getHmacRequest(any(), any(), any(), any())).thenReturn(new Gson().toJson(CommonTestObjects.getResponseTestObject()));
        when(userService.create(any())).thenReturn(user);
        when(addressService.createAddress(any())).thenReturn(address);
        when(homeRepository.save(any())).thenReturn(null);
        Home results = homeService.create();
        assertNull(results);

    }

    @Test
    public void createHomeWhenUserIsNull() throws Exception {
        Address address = CommonTestObjects.getAddressObject();
        User user = new User();
        user.setActive(true);
        user.setId(1L);
        Home home = CommonTestObjects.getHomeTestObject();
        home.setUser(user);
        when(residentService.getHmacRequest(any(), any(), any(), any())).thenReturn(new Gson().toJson(CommonTestObjects.getResponseTestObject()));
        when(userService.create(any())).thenReturn(null);
        when(addressService.createAddress(any())).thenReturn(address);
        when(homeRepository.save(any())).thenReturn(null);
        Home results = homeService.create();
        assertNull(results);

    }

    @Test
    public void createInstitutional() throws Exception {
        Address address = CommonTestObjects.getAddressObject();
        User user = new User();
        user.setActive(true);
        user.setId(1L);
        Home home = CommonTestObjects.getHomeTestObject();
        home.setUser(user);
        when(residentService.getHmacRequest(any(), any(), any(), any())).thenReturn(new Gson().toJson(CommonTestObjects.getResponseTestObject()));
        when(userService.create(any())).thenReturn(user);
        when(addressService.createAddress(any())).thenReturn(address);
        when(homeRepository.save(any())).thenReturn(null);
        Home results = homeService.createInstitutional();
        assertNull(results);
    }

    @Test
    public void updateHome() throws Exception {
        when(homeRepository.findById(any())).thenReturn(Optional.ofNullable(CommonTestObjects.getHomeTestObject()));
        when(homeRepository.save(any())).thenReturn(CommonTestObjects.getHomeTestObject());
        Home resp = homeService.update(CommonTestObjects.getHomeTestObject());
        assertNotNull(resp);
    }

    @Test
    public void updateHomeWithException() {
        Home home = CommonTestObjects.getHomeTestObject();
        assert home != null;
        home.setId(null);
        Exception ex = assertThrows(Exception.class, () -> {
            homeService.update(home);
        }, "Id shouldn't be null");
        Assertions.assertEquals("Id shouldn't be null", ex.getMessage());

    }
}
