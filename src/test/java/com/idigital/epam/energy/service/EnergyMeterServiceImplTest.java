package com.idigital.epam.energy.service;

import com.idigital.epam.energy.common.CommonTestObjects;
import com.idigital.epam.energy.entity.EnergyMeter;
import com.idigital.epam.energy.entity.Home;
import com.idigital.epam.energy.enums.BuildingType;
import com.idigital.epam.energy.payload.EnergyMeterRequest;
import com.idigital.epam.energy.payload.EnergyResponse;
import com.idigital.epam.energy.payload.Response;
import com.idigital.epam.energy.repository.EnergyMeterRepository;
import com.idigital.epam.energy.repository.HomeRepository;
import com.idigital.epam.energy.service.Impl.EnergyMeterServiceImpl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;


public class EnergyMeterServiceImplTest {


    @InjectMocks
    EnergyMeterServiceImpl energyMeterService;

    @Mock
    EnergyMeterRepository energyMeterRepository;

    @Mock
    HomeRepository homeRepository;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Test get All ")
    public void  testGetAll(){
        List<EnergyMeter> energyMeterList = new ArrayList<>(Collections.singletonList(CommonTestObjects.getEnergyMeter()));
        when(energyMeterRepository.findAll()).thenReturn(energyMeterList);
        List<EnergyMeter> res = energyMeterService.getAll();
        assertNotNull(energyMeterList);
        assertEquals(res,energyMeterList);
    }

    @Test
    @DisplayName("Test get energy meter by id ")
    public void  testGetById(){
        EnergyMeter energyMeter = CommonTestObjects.getEnergyMeter();
        when(energyMeterRepository.findById(anyLong())).thenReturn(Optional.of(energyMeter));
        EnergyMeter res = energyMeterService.getById(1L);
        assertNotNull(energyMeter);
        assertNotNull(res);
    }

    @Test
    @DisplayName("Test get All Energy Meter")
    public void testGetAllEnergyMeter(){
        List<EnergyResponse> energyResponsesList = new ArrayList<>(Collections.singletonList(CommonTestObjects.getEnergyResponse()));
        when(energyMeterRepository.getAllEnergy()).thenReturn(energyResponsesList);
        List<EnergyResponse> res = energyMeterService.getAllEnergyMeter();
        assertNotNull(energyResponsesList);
        assertEquals(res,energyResponsesList);
    }

    @Test
    @DisplayName("Test get home by id")
    public void testGetByHomeId(){
        EnergyMeter energyMeter = CommonTestObjects.getEnergyMeter();
        assertNotNull(energyMeter);
        when(energyMeterRepository.findByHomeId(anyLong())).thenReturn(energyMeter);
        EnergyMeter response = energyMeterService.getByHomeId(22L);
        assertNotNull(response);
    }

    @Test
    @DisplayName("Test Create Energy Meter")
    public void testCreateEnergyMeter() throws Exception {

        EnergyMeterRequest energyMeterRequest = EnergyMeterRequest.builder()
                .energyConsumption(13)
                .homeId(1L)
                .institutional("Institutional")
                .residential("resident")
                .build();
        Home home = Home.builder().id(1L).homeCode(1L).buildingType(BuildingType.RESIDENTIAL).build();
        when(homeRepository.findById(1L)).thenReturn(Optional.of(home));
        Response resp = energyMeterService.create(energyMeterRequest);
        assertNotNull(resp);
    }

    @Test
    @DisplayName("Energy Meter Calculation")
    public void EnergyMeterCalculation(){
        EnergyMeter energyMeter = CommonTestObjects.getEnergyMeter();
        List<EnergyMeter> energyMeterList =  new ArrayList<>(Collections
                .singletonList(energyMeter));
        assertNotNull(energyMeterList);
        when(energyMeterRepository.findAll()).thenReturn(Collections.singletonList(energyMeter));
        when(energyMeterService.getAll()).thenReturn(energyMeterList);
        energyMeterService.EnergyMeterCalculation();
    }

    @Test
    @DisplayName("Energy Meter Calculation With null previous reading")
    public void EnergyMeterCalculationWithNullPreviousReading(){
        EnergyMeter energyMeter = CommonTestObjects.getEnergyMeter();
        energyMeter.setPreviousReading(null);
        List<EnergyMeter> energyMeterList =  new ArrayList<>(Collections
                .singletonList(energyMeter));
        assertNotNull(energyMeterList);
        when(energyMeterRepository.findAll()).thenReturn(Collections.singletonList(energyMeter));
        when(energyMeterService.getAll()).thenReturn(energyMeterList);
        energyMeterService.EnergyMeterCalculation();
    }

    @Test
    @DisplayName("Test Update EnergyMeter")
    public void testUpdateEnergyMeter() throws Exception {
        EnergyMeter energyMeter = CommonTestObjects.getEnergyMeter();
        when(energyMeterRepository.save(any())).thenReturn(energyMeter);
        EnergyMeter e = energyMeterService.update(energyMeter);
        assertNotNull(e);

    }

    @Test
    @DisplayName("Test Update EnergyMeter with Throws Exception")
    public void testUpdateEnergyMeterThrowsException(){
        EnergyMeter energyMeterTest = CommonTestObjects.getEnergyMeter();
        energyMeterTest.setId(null);
        Exception ex = assertThrows(Exception.class, () -> energyMeterService.update(energyMeterTest),"Id shouldn't be null");
        assertEquals("Id shouldn't be null", ex.getMessage());
    }

    @Test
    @DisplayName("Test delete energy meter")
    public void testDeleteEnergyMeter() throws Exception {
        EnergyMeter energyMeter = CommonTestObjects.getEnergyMeter();
        doNothing().when(energyMeterRepository).deleteById(any());
        assertNotNull(energyMeter);
        energyMeterService.delete(energyMeter);
    }
}
