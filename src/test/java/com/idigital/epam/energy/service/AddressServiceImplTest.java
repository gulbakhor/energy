package com.idigital.epam.energy.service;


import com.idigital.epam.energy.common.CommonTestObjects;
import com.idigital.epam.energy.entity.Address;
import com.idigital.epam.energy.repository.AddressRepository;
import com.idigital.epam.energy.service.Impl.AddressServiceImpl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class AddressServiceImplTest {

    @InjectMocks
    AddressServiceImpl addressService;

    @Mock
    private AddressRepository addressRepository;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Test create Address")
    public void testCreateAddress(){
        Address address = CommonTestObjects.getAddressObject();
        when(addressRepository.save(any())).thenReturn(address);
        Address res = addressService.createAddress(address);
        assertNotNull(res);
    }

    @Test
    @DisplayName("Test delete Address")
    public void testDeleteAddress(){
        // for all void methods/functions use doNothing()
        doNothing().when(addressRepository).deleteById(anyLong());
        boolean returnT = addressService.deleteAddress(4L);
        assertTrue(returnT);
    }

    @Test
    @DisplayName("Test delete Address with exception")
    public void testDeleteAddressWithException(){
        doThrow(RuntimeException.class).when(addressRepository).deleteById(anyLong());
        Exception ex = assertThrows(Exception.class, () -> addressRepository.deleteById(anyLong()));
        boolean returnT = addressService.deleteAddress(4L);
        assertNotNull(ex);
        assertFalse(returnT);
    }

    @Test
    @DisplayName("Test update Address")
    public void testUpdateAddress(){
        when(addressRepository.findById(anyLong())).thenReturn(Optional.of(CommonTestObjects.getAddressObject()));
        when(addressRepository.save(any())).thenReturn(null);
        boolean returnT = addressService.updateAddress(CommonTestObjects.getAddressObject(),3L);
        assertTrue(returnT);
    }


    @Test
    @DisplayName("Test update Address with exception")
    public void testUpdateAddressWithException(){
        doThrow(RuntimeException.class).when(addressRepository).findById(anyLong());
        Exception ex = assertThrows(Exception.class, () -> addressRepository.findById(anyLong()));
        boolean returnT = addressService.updateAddress(CommonTestObjects.getAddressObject(),3L);
        assertNotNull(ex);
        assertFalse(returnT);
    }

    @Test
    @DisplayName("Test get Address")
    public void testGetAddress(){
        when(addressRepository.findById(anyLong())).thenReturn(Optional.of(CommonTestObjects.getAddressObject()));
        Address address = addressService.getAddress(7L);
        assertNotNull(address);
    }

}
