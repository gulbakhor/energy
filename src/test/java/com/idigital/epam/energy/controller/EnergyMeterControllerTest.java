package com.idigital.epam.energy.controller;

import com.idigital.epam.energy.common.CommonTestObjects;
import com.idigital.epam.energy.entity.EnergyMeter;
import com.idigital.epam.energy.payload.EnergyResponse;
import com.idigital.epam.energy.payload.Response;
import com.idigital.epam.energy.repository.EnergyMeterRepository;
import com.idigital.epam.energy.service.EnergyMeterService;
import com.idigital.epam.energy.service.HomeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

public class EnergyMeterControllerTest {

    @InjectMocks
    EnergyMeterController energyMeterController;

    @Mock
    EnergyMeterService energyMeterService;

    @Mock
    EnergyMeterRepository energyMeterRepository;

    @Mock
    HomeService homeService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Test create Energy Meter")
    public void testCreateEnergyMeter() throws Exception {
        when(energyMeterService.create(any())).thenReturn(Response.builder().build());
        ResponseEntity<Response> responseResponseEntity = energyMeterController.createEnergyMeter(CommonTestObjects.getEnergyMeterRequest());
        assertNotNull(responseResponseEntity);
    }


    @Test
    @DisplayName("Test get All")
    public void testGetAll() {
        List<EnergyResponse> energyResponse = Collections.singletonList(CommonTestObjects.getEnergyResponse());
        assertNotNull(energyResponse);
        when(energyMeterService.getAllEnergyMeter()).thenReturn(energyResponse);
        ResponseEntity<List<EnergyResponse>> responseEntity = energyMeterController.getAll();
        assertNotNull(responseEntity);
    }

    @Test
    @DisplayName("Test get by Energy Meter Id")
    public void testGetEnergyMeterId() {
        List<EnergyMeter> energyMeter = Collections.singletonList(CommonTestObjects.getEnergyMeter());
        assertNotNull(energyMeter);
        when(energyMeterService.getById(any())).thenReturn(CommonTestObjects.getEnergyMeter());
        ResponseEntity<EnergyMeter> responseEntity = energyMeterController.getEnergyMeterById(1L);
        assertNotNull(responseEntity);
    }

    @Test
    @DisplayName("Test get by Energy Meter HomeId")
    public void getByHomeId() {
        when(energyMeterService.getByHomeId(anyLong())).thenReturn(CommonTestObjects.getEnergyMeter());
        ResponseEntity<?> responseEntity = energyMeterController.getByHomeId(11L);
        assertNotNull(responseEntity);
    }

}
