package com.idigital.epam.energy.controller;


import com.idigital.epam.energy.common.CommonTestObjects;
import com.idigital.epam.energy.entity.Address;
import com.idigital.epam.energy.entity.Home;
import com.idigital.epam.energy.entity.User;
import com.idigital.epam.energy.enums.BuildingType;
import com.idigital.epam.energy.repository.HomeRepository;
import com.idigital.epam.energy.service.DTO.HomeResponse;
import com.idigital.epam.energy.service.HomeService;
import com.idigital.epam.energy.service.UserService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class HomeControllerTest {
    @Mock
    HomeService homeService;
    @Mock
    HomeRepository homeRepository;
    @Mock
    UserService userService;
    @InjectMocks
    HomeController homeController;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("get all users with homes")
    public void testGetAllUsersWithHomes() throws Exception {

        // the mock => always create the mock first using @Mock
        Home home = CommonTestObjects.getHomeTestObject();
        assertNotNull(home);

        when( homeService.create()).thenReturn(home);
        when( homeService.createInstitutional()).thenReturn(home);
        when( homeService.getAll()).thenReturn(Collections.singletonList(home));

        // after mocking => call the function that was injected using @InjectMocks
        ResponseEntity<List<Home>> responseEntity = homeController.getAll();
        assertNotNull(responseEntity);


    }

    @Test
    @DisplayName("get homes with current user")
    public void testGetHomesWithCurrentUser() throws Exception {
        List<HomeResponse> responses = Collections.singletonList(CommonTestObjects.getHomeResponseTestObject());
        // the mock => always create the mock first using @Mock
        User user = CommonTestObjects.getUserObject();
        assertNotNull(user);
        when( userService.getCurrentUser()).thenReturn(user);
        when(homeRepository.findHomeListByUserId(anyString())).thenReturn(responses);
        // after mocking => call the function that was injected using @InjectMocks
        ResponseEntity entity = homeController.getResultByCardNumber();
        assertNotNull(entity);
    }

    @Test
    @DisplayName("get all homes and responses")
    public void testGetHomesList(){
        List<HomeResponse> responses = Collections.singletonList(CommonTestObjects.getHomeResponseTestObject());
        when(homeService.getHomesList()).thenReturn(responses);
        ResponseEntity<List<HomeResponse>> homes = homeController.getHomesList();
        assertNotNull(homes);
    }
}