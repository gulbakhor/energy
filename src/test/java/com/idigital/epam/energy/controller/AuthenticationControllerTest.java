package com.idigital.epam.energy.controller;


import com.idigital.epam.energy.common.CommonTestObjects;
import com.idigital.epam.energy.entity.User;
import com.idigital.epam.energy.service.HomeService;
import com.idigital.epam.energy.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;


public class AuthenticationControllerTest {

    @InjectMocks
    AuthenticationController authenticationController;

    @Mock
    private UserService userService;

    @Mock
    HomeService homeService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName(" Test authentication ")
    public void testSignInToTheServer() throws Exception {

    when(userService.authentication(any())).thenReturn("232");
        ResponseEntity<?> responseEntity = authenticationController.signInToTheServer(CommonTestObjects.getUserMaxsus());
        assertNotNull(responseEntity);
    }


    @Test
    @DisplayName("Test Registration")
    public void testSignUpToServer() throws Exception {
        when(userService.create(any())).thenReturn(CommonTestObjects.getUserObject());
        ResponseEntity<?> responseEntity = authenticationController.signUpToServer(CommonTestObjects.getUserMaxsus());
        assertNotNull(responseEntity);

    }


    @Test
    @DisplayName("Test get Current User")
    public void testGetCurrenUser(){
        when(userService.getCurrentUser()).thenReturn(CommonTestObjects.getUserObject());
        ResponseEntity<User> userResponseEntity = authenticationController.getCurrentUser();
        assertNotNull(userResponseEntity);
    }


}
