//package com.idigital.epam.energy.controller;
//
//import com.idigital.epam.energy.common.CommonTestObjects;
//import com.idigital.epam.energy.entity.User;
//import com.idigital.epam.energy.service.UserService;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.server.ResponseStatusException;
//
//import java.util.Collections;
//import java.util.List;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.ArgumentMatchers.anyLong;
//import static org.mockito.Mockito.when;
//
//
//public class UserControllerTest {
//
//    @Mock
//    UserService userService;
//
//    @InjectMocks
//    private UserController userController;
//
//    @BeforeEach
//    public void setUp(){
//        MockitoAnnotations.openMocks(this);
//    }
//
//    @Test
//    @DisplayName("Test get by id")
//    public void getById(){
//        User user = CommonTestObjects.getUserObject();
//        when(userService.getById(anyLong())).thenReturn(user);
//        ResponseEntity<User> response = userController.getById(7L);
//        assertNotNull(response);
//        assertEquals(new ResponseEntity<>(user, HttpStatus.OK),response);
//    }
//
//    @Test
//    @DisplayName("Test get by id with bad request")
//    public void getByIdWithBadRequest(){
//        User user = CommonTestObjects.getUserObject();
//        when(userService.getById(anyLong())).thenReturn(null);
//        Exception ex = assertThrows(ResponseStatusException.class, () -> {
//            userController.getById(7L);
//        }, HttpStatus.BAD_REQUEST.toString());
//        Assertions.assertEquals(HttpStatus.BAD_REQUEST.toString(), ex.getMessage());
//    }
//
//    @Test
//    @DisplayName("Test get all users")
//    public void testGetAllUsers() {
//        List<User> users = Collections.singletonList(CommonTestObjects.getUserObject());
//        when(userService.getAll()).thenReturn(users);
//        ResponseEntity<List<User>> response = userController.getAllUser();
//        assertNotNull(response);
//        assertEquals(new ResponseEntity<>(users, HttpStatus.OK),response);
//
//    }
//}
