//package com.idigital.epam.energy.controller.IntegrationTests;
//
//import com.idigital.epam.energy.ElectricityBillingSystemRestApplication;
//import com.idigital.epam.energy.common.CommonTestObjects;
//import com.idigital.epam.energy.entity.Home;
//import com.idigital.epam.energy.entity.User;
//import com.idigital.epam.energy.repository.HomeRepository;
//import com.idigital.epam.energy.service.HomeService;
//import com.idigital.epam.energy.service.UserService;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//
//import org.mockito.Mockito;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.boot.web.server.LocalServerPort;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//import org.springframework.web.client.RestTemplate;
//
//import java.util.Collections;
//import java.util.List;
//import java.util.Objects;
//
//import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
//import static org.junit.jupiter.api.Assertions.*;
//
//
//@SpringBootTest(classes = ElectricityBillingSystemRestApplication.class,
//        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//public class HomeControllerTest {
//
//    @LocalServerPort
//    private int port;
//
//    private static final String BASE_URL = "http://localhost:8080";
//
//    @Autowired
//    private TestRestTemplate restTemplate;
//
//    @Autowired
//    HomeService homeService;
//    @Autowired
//    HomeRepository homeRepository;
//
//    @Autowired
//    UserService userService;
//
//
//    HttpHeaders headers = new HttpHeaders();
//
//    @Test
//    @DisplayName("Test All Users With Homes")
//    public void testGetAllUsersWithHomes() {
//
//        assertNotNull(this.restTemplate
//                .getForObject(this.getUrl("getAllUsersWithHomes"), Object.class));
//
//        ResponseEntity<List<Home>> entity = new ResponseEntity<>(null,headers,HttpStatus.OK);
//
//        ResponseEntity<String> response = this.restTemplate.exchange(
//                this.getUrl("/getAllUsersWithHomes"), HttpMethod.GET, entity, String.class);
//
//        String actual = response.getHeaders().toString();
//
//        assertTrue(actual.contains("Connection"));
//    }
//
//
//    @Test
//    @DisplayName("Test Get Homes With Current User")
//    public void testGetHomesWithCurrentUser() {
//
//        assertNotNull(this.restTemplate
//                .getForObject(this.getUrl("getHomesWithCurrentUser"), Object.class));
//
//        ResponseEntity<List<Home>> entity = new ResponseEntity<>(null,headers,HttpStatus.OK);
//
//        ResponseEntity<String> response = this.restTemplate.exchange(
//                this.getUrl("getHomesWithCurrentUser"), HttpMethod.GET, entity, String.class);
//
//        String actual = response.getHeaders().toString();
//
//        assertTrue(actual.contains("Connection"));
//    }
//
//    private String getUrl(String path){
//        return "http://localhost:" + port + "/api/home/"+ path;
//    }
//}
