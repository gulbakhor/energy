package com.idigital.epam.energy.controller;

import com.idigital.epam.energy.common.CommonTestObjects;
import com.idigital.epam.energy.entity.Billing;
import com.idigital.epam.energy.entity.EnergyMeter;
import com.idigital.epam.energy.service.BillingService;
import com.idigital.epam.energy.service.EnergyMeterService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;


public class BillingControllerTest {

    @InjectMocks
    BillingController billingController;

    @Mock
    BillingService billingService;

    @Mock
    EnergyMeterService energyMeterService;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetAllBilling() throws Exception {
        List<Billing> billings = Collections.singletonList(CommonTestObjects.getBilling());
        assertNotNull(billings);
        when(billingService.getAll()).thenReturn(billings);
        List<Billing> bills = billingController.getAll();
        assertNotNull(bills);
    }

    @Test
    public void testGetBillingByHomeId() throws Exception {
        List<Billing> billings = Collections.singletonList(CommonTestObjects.getBilling());
        assertNotNull(billings);
        when(billingService.findByHomeId(anyLong())).thenReturn(billings);
        List<Billing> bills = billingController.getBillingByHomeId(1L);
        assertNotNull(bills);
    }

    @Test
    public void testCreateBilling() throws Exception {
        List<Billing> billings = Collections.singletonList(CommonTestObjects.getBilling());
        assertNotNull(billings);
        when(billingService.create(any())).thenReturn(billings.get(0));
        Billing bills = billingController.createBilling(billings.get(0));
        assertNotNull(bills);
    }

    @Test
    public void testUpdateBilling() throws Exception {
        List<Billing> billings = Collections.singletonList(CommonTestObjects.getBilling());
        assertNotNull(billings);
        when(billingService.update(any())).thenReturn(billings.get(0));
        Billing bills = billingController.updateBilling(billings.get(0));
        assertNotNull(bills);
    }

    @Test
    public void testDeleteBilling() throws Exception {
        List<Billing> billings = Collections.singletonList(CommonTestObjects.getBilling());
        assertNotNull(billings);
        doNothing().when(billingService).deleteById(anyLong());
         billingController.deleteBilling(1L);
    }

    @Test
    public void testGetBillingBasedOnId() {
        EnergyMeter energyMeter = CommonTestObjects.getEnergyMeter();
        assertNotNull(energyMeter);
        when(energyMeterService.getById(anyLong())).thenReturn(energyMeter);
        EnergyMeter e = billingController.getBillingBasedOnId(1L);
        assertNotNull(e);
    }

    @Test
    public void testGetBillingById(){
        Billing billing = CommonTestObjects.getBilling();
        assertNotNull(billing);
        when(billingService.getById(anyLong())).thenReturn(billing);
        Billing b = billingController.getById(1L);
        assertNotNull(b);
    }

}
