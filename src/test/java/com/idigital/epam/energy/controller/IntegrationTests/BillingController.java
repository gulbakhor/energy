//package com.idigital.epam.energy.controller.IntegrationTests;
//
//import com.idigital.epam.energy.common.CommonTestObjects;
//import com.idigital.epam.energy.entity.Billing;
//import com.idigital.epam.energy.entity.EnergyMeter;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import org.springframework.web.client.RestTemplate;
//
//import java.util.List;
//import java.util.UUID;
//
//import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
//
//public class BillingController {
//
//    private static final RestTemplate REST_TEMPLATE = new RestTemplate();
//    private static final String URL = "http://localhost:8080/api/billing";
//
//
//    @Test
//    @DisplayName("Test Billing")
//    public void TestBilling(){
//
//        Billing billing = CommonTestObjects.getBilling();
//        //billing.setSum(UUID.randomUUID().variant());
//        Billing created = REST_TEMPLATE.postForObject(URL + "/save", billing, Billing.class);
//
//        verify(billing, created);
//
//        Billing byId = REST_TEMPLATE.getForObject(URL + "/getOne/" + created.getId(), Billing.class);
//        verify(created, byId);
//
//
//
//        List<?> billings = REST_TEMPLATE.getForObject(URL + "/getAllBillings", List.class);
////        assertThat(billings).isNotEmpty();
//        assertThat(billing).isNotNull();
//    }
//
//    private void verify(Billing billing, Billing created) {
//        assertThat(billing).isNotNull().isInstanceOf(Billing.class);
//        assertThat(created.getId()).isNotNull();
//        assertThat(created.getSum()).isEqualTo(billing.getSum());
//        assertThat(created.getAmountEnergyConsumption()).isEqualTo(billing.getAmountEnergyConsumption());
//    }
//
//}
