
FROM openjdk:11-jre-slim as backend


ENV TZ Asia/Tashkent


# Add Labels
LABEL project="Electricity Billing"
LABEL maintainer="Bahor"

# Add a volume pointing to /tmp
VOLUME /tmp

# Make port 8080 available to the world outside this container
EXPOSE 8080

# The fat jar
ARG JAR_FILE=target/*.jar

# Add the application's jar to the container
ADD ${JAR_FILE} app.jar

# Run the jar file
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]

